import React from "react";
import ReactDOM from "react-dom";
import TextInput from "./lib/index";

ReactDOM.render(
  <div>
    Hello world <TextInput label={"Demo"} placeholder="ener name" />
  </div>,
  document.getElementById("root")
);
