import React from "react";

const TextInput = ({ type = "text", label, value, onChange }) => (
  <div style={{ marginBottom: "1rem" }}>
    {label && (
      <label
        style={{ display: "block", color: "red" }}
        className="simple-text-label"
      >
        {label}
      </label>
    )}
    <input
      type={type}
      style={{
        display: "inline-block",
        marginBottom: "0.5rem",
        fontSize: "16px",
        fontWeight: "400",
        color: "rgb(33, 37, 41)",
      }}
      value={value}
      onChange={(e) => onChange && onChange(e.target.value)}
    />
  </div>
);

export default TextInput;
